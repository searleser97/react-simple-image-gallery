import React, { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios';
const server_url = "http://192.168.0.100:8000";

async function delay(ms: number) {
  return await new Promise(resolve => setTimeout(resolve, ms));
}

interface SSResponse {
  screenshots: string[];
}

const App: React.FC = () => {
  const [ScreenshotsList, setScreenshotsList] = useState();
  const requestScreenshots = async () => {
    while (true) {
      const response = await axios.get<SSResponse>(server_url + '/getScreenShots');
      const screenshots_url = response.data.screenshots;
      const ss = screenshots_url.map(url => {
        const img = server_url + '/' + url;
        return (
          <div key={url} style={{margin: "10px 10px 0 10px", background: "url(" + img + ")", backgroundSize: "100% 100%", backgroundRepeat: "no-repeat", width: '400px', height: '250px' }} ></div>
        );
      });
      setScreenshotsList(ss);
      await delay(5000);
    }
  };
  useEffect(() => {
    requestScreenshots();
  });
  return (
    <div style={{display: "flex", flexWrap: "wrap", flexShrink: 0, width: "100vw", height: "100vh", overflowY: "auto"}}>{ScreenshotsList}</div>
  );
}

export default App;
